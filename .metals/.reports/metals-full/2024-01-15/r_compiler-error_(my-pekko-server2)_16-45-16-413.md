file://<WORKSPACE>/src/main/scala/fr/Main.scala
### scala.reflect.internal.Types$TypeError: illegal cyclic reference involving object Directives

occurred in the presentation compiler.

action parameters:
uri: file://<WORKSPACE>/src/main/scala/fr/Main.scala
text:
```scala
package org.openapitools.server.api

import org.apache.pekko
import pekko.actor.typed.ActorSystem
import pekko.actor.typed.scaladsl.Behaviors
import pekko.http.scaladsl.Http
import pekko.http.scaladsl.model._
import pekko.http.scaladsl.server.Directives._
import scala.io.StdIn

import pekko.util.Timeout
import scala.concurrent.duration._

import pekko.http.scaladsl.marshalling.ToEntityMarshaller
import pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller

import pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

import org.apache.pekko.http.scaladsl.server.Route
import org.openapitools.server.api.{DefaultApi, DefaultApiService, DefaultApiMarshaller}
import org.openapitools.server.api._

import spray.json.RootJsonFormat


import org.openapitools.server.model.AnnoncesPostRequest
import org.openapitools.server

object HttpServerRoutingMinimal {

  def main(args: Array[String]): Unit = {

    implicit val system = ActorSystem(Behaviors.empty, "my-system")
    // needed for the future flatMap/onComplete in the end
    implicit val executionContext = system.executionContext
    /*
    val route =
    path("annonces" / Segment) { annonceId =>
      get {
        // Get a specific phone advertisement
        // Use the actor to handle the request and respond with the result
        // The response can be converted to HTTP response format
        // For simplicity, returning a dummy response
        complete(s"Getting phone advertisement with ID: $annonceId")
      } ~
      put {
        // Update a specific phone advertisement
        // Use the actor to handle the request and respond with the result
        // The response can be converted to HTTP response format
        // For simplicity, returning a dummy response
        complete(s"Updating phone advertisement with ID: $annonceId")
      }
    } ~
    path("annonces") {
      get {
        // Get a list of all phone advertisements
        // Use the actor to handle the request and respond with the result
        // The response can be converted to HTTP response format
        // For simplicity, returning a dummy response
        complete("Getting all phone advertisements")
      } ~
      post {
        // Create a new phone advertisement
        // Use the actor to handle the request and respond with the result
        // The response can be converted to HTTP response format
        // For simplicity, returning a dummy response
        complete("Creating a new phone advertisement")
      }
    }*/
    

    val bindingFuture = Http().newServerAt("localhost", 8080).bind(DefaultApi.route)

    println(s"Server now online. Please navigate to http://localhost:8080/hello\nPress RETURN to stop...")
    StdIn.readLine() // let it run until user presses return
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate()) // and shutdown when done
  }
}


```



#### Error stacktrace:

```

```
#### Short summary: 

scala.reflect.internal.Types$TypeError: illegal cyclic reference involving object Directives