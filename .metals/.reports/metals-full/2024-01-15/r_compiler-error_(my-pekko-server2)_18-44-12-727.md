file://<WORKSPACE>/src/main/scala/fr/Main.scala
### scala.reflect.internal.Types$TypeError: illegal cyclic reference involving object DefaultJsonProtocol

occurred in the presentation compiler.

action parameters:
uri: file://<WORKSPACE>/src/main/scala/fr/Main.scala
text:
```scala
package org.openapitools.server.api

import org.apache.pekko
import pekko.actor.typed.ActorSystem
import pekko.actor.typed.scaladsl.Behaviors
import pekko.http.scaladsl.Http
import pekko.http.scaladsl.model._
import pekko.http.scaladsl.server.Directives._
import scala.io.StdIn

import pekko.actor.typed.{ActorSystem, ActorRef}
import pekko.actor.typed.scaladsl.Behaviors

import pekko.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

import pekko.http.scaladsl.marshalling.ToEntityMarshaller
import pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller

import pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._

import org.apache.pekko.http.scaladsl.server.Route
import org.openapitools.server.api.{AnnoncesApi, AnnoncesApiService, AnnoncesApiMarshaller}
import org.openapitools.server.api._

import spray.json.RootJsonFormat

import scala.util.Failure
import scala.util.Success

import pekko.actor.typed.scaladsl.AskPattern._

import pekko.http.scaladsl.model.StatusCodes

import org.openapitools.server.model.Annonces
import org.openapitools.server
import org.openapitools.server.api.AnnoncesActor

import pekko.http.scaladsl.marshalling.ToEntityMarshaller
import pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller

import pekko.actor.typed.{ActorSystem, ActorRef}
import pekko.actor.typed.scaladsl.Behaviors

import pekko.http.scaladsl.Http
import pekko.http.scaladsl.server.Route
import pekko.http.scaladsl.server.StandardRoute
import pekko.http.scaladsl.server.RouteResult

import pekko.http.scaladsl.server.Directives._
import pekko.http.scaladsl.model.StatusCodes
import pekko.http.scaladsl.marshalling.ToEntityMarshaller
import pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller

import pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._

import scala.io.StdIn

import pekko.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

import org.openapitools.server.api._

import pekko.actor.typed.scaladsl.AskPattern._

import pekko.actor.typed.Behavior
import spray.json.RootJsonFormat
import scala.util.Failure
import scala.util.Success



object Main extends App {
  implicit val system = ActorSystem(Behaviors.empty, "my-system")
  implicit val executionContext = system.executionContext
  
  val annoncesActor: ActorRef[AnnoncesActor.Command] = system

  def routeFromFutureRoute(futureroute: Future[Route]): Route = (context) => futureroute.flatMap(route => route(context)) 

  object Api extends AnnoncesApiService {
    implicit val timeout: Timeout = 3.seconds

    override def getAnnonceById(id: String)(implicit toEntityMarshallerRaccourciarray: ToEntityMarshaller[Annonces]): Route = routeFromFutureRoute {
      annoncesActor.ask(ref => AnnoncesActor.GetAnnonceById(id, ref)).map { ma =>
        (ma match {
        case Some(annonce) => complete((200,annonce))
        case None => complete((404, s"Annonce with ID $id not found"))
      })
      }
    }

    override def getAllAnnonces()(implicit toEntityMarshallerRaccourciarray: ToEntityMarshaller[Annonces]): Route = routeFromFutureRoute {
      annoncesActor.ask(ref => AnnoncesActor.GetAllAnnonces(ref)).map {
        case Some(annonce) => complete((200, annonce))
        case None => complete((404, "No annonces found"))
      }
    }

    override def createAnnonce(id: String,brand: String, model: String, price: Float)(implicit toEntityMarshallerRaccourciarray: ToEntityMarshaller[Annonces]): Route = routeFromFutureRoute {
      annoncesActor.ask(ref => AnnoncesActor.CreateAnnonce(id,brand,model,price, ref)).map {
        case true => complete((201, "Annonce created successfully"))
        case false => complete((400, "Failed to create annonce"))
      }
    }
  }

  object Marsh extends AnnoncesApiMarshaller {
    implicit val r = jsonFormat4(Annonces.apply)
    override implicit def fromEntityUnmarshallerAnnonce: FromEntityUnmarshaller[Annonces] = r
    override implicit def toEntityMarshallerAnnonce: ToEntityMarshaller[Annonces] = r

    override implicit def toEntityMarshallerAnnonceSeq: ToEntityMarshaller[Seq[Annonces]] = immSeqFormat(r)
  }

  val api = new AnnoncesApi(Api, Marsh)

  val bindingFuture = Http().newServerAt("localhost", 8080).bind(api.route)

  println(s"Server now online. Please navigate to http://localhost:8080/hello\nPress RETURN to stop...")
  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done
}



```



#### Error stacktrace:

```

```
#### Short summary: 

scala.reflect.internal.Types$TypeError: illegal cyclic reference involving object DefaultJsonProtocol