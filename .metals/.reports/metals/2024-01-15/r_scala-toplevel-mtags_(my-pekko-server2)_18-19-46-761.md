error id: file://<WORKSPACE>/src/main/scala/fr/Actor.scala:[36..37) in Input.VirtualFile("file://<WORKSPACE>/src/main/scala/fr/Actor.scala", "package org.openapitools.server.api._

import org.apache.pekko
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.http.scaladsl.server.Directives._

import pekko.actor.typed.{ActorRef, Behavior,ActorSystem}
import pekko.actor.typed.scaladsl.{Behaviors,AbstractBehavior,ActorContext}
import scala.util.Random


// Define a case class representing a phone advertisement

final case class Annonce(d: String, brand: String, model: String, price: Float)

object AnnoncesActor {

  // Define messages for the actor
  sealed trait Command
  case class GetAnnonceById(id: String, replyTo: ActorRef[Option[Annonce]]) extends Command
  case class GetAllAnnonces(replyTo: ActorRef[Option[List[Annonce]]]) extends Command
  case class CreateAnnonce(annonce: Annonce, replyTo: ActorRef[Boolean]) extends Command

  // Behavior for the actor
  def apply(): Behavior[Command] = Behaviors.setup(context => new AnnoncesActor(context))
}

  // Inner class for the behavior
  class AnnoncesActor(context: ActorContext[AnnoncesActor.Command]) extends AbstractBehavior[AnnoncesActor.Command](context) {

    context.log.info("Hello AnnoncesActor !")
    import AnnoncesActor._

    var annonces: Map[String, Annonce] = Map("1" -> Annonce("1", "Brand1", "Model1", 1000.0f))
    
    override def onMessage(msg: Command): Behavior[Command] =
      msg match {
        case GetAnnonceById(id, replyTo) =>
          replyTo ! annonces.get(id)
          this

        case GetAllAnnonces(replyTo) =>
          replyTo ! Some(annonces.values.toList)
          this

        case CreateAnnonce(annonce, replyTo) =>
          replyTo ! {
            context.log.info(s"Creating Annonce: $annonce")
            if (annonces.contains(annonce.d)) {
              context.log.info(s"Error: Annonce with ID ${annonce.d} already exists.")
              false
            } else {
              context.log.info(s"Adding Annonce: $annonce")
              annonces += (annonce.d -> annonce)
              true
            }
          }
          this
      }
  }
")
file://<WORKSPACE>/src/main/scala/fr/Actor.scala
file://<WORKSPACE>/src/main/scala/fr/Actor.scala:1: error: expected identifier; obtained uscore
package org.openapitools.server.api._
                                    ^
#### Short summary: 

expected identifier; obtained uscore