```mermaid
sequenceDiagram
  participant Client
  participant AnnoncesApi
  participant AnnoncesActor

  Client ->> AnnoncesApi: POST /annonces
  AnnoncesApi ->> AnnoncesActor: CreateAnnonce(id, brand, model, price, ref)
  AnnoncesActor -->> AnnoncesApi: "Annonce created successfully" or "Failed to create annonce"
  AnnoncesApi -->> Client: 201 Created or 400 Bad Request
