```mermaid
sequenceDiagram
  participant Client
  participant AnnoncesApi
  participant AnnoncesActor

  Client ->> AnnoncesApi: GET /annonces/{id}
  AnnoncesApi ->> AnnoncesActor: GetAnnonceById(id, ref)
  AnnoncesActor -->> AnnoncesApi: Option[Annonce]
  AnnoncesApi -->> Client: 200 OK (with Annonce data) or 404 Not Found
