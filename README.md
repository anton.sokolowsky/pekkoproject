# Phone Sale API

API for phone sales


    ## API

          ### Default

          |Name|Role|
          |----|----|
          |`org.openapitools.server.api.AnnoncesController`|akka-http API controller|
          |`org.openapitools.server.api.AnnoncesApi`|Representing trait|
              |`org.openapitools.server.api.AnnoncesApiImpl`|Default implementation|

                * `GET /annonces/{annonceId}` - Get a specific phone advertisement
                * `PUT /annonces/{annonceId}` - Update a specific phone advertisement // pas fait
                * `GET /annonces` - Get a list of all phone advertisements
                * `POST /annonces` - Create a new phone advertisement



Pour lancer la partie server : 

faire sbt run et taper 2

Pour lancer la démonstration de l'acteur :

faire sbt run et taper 1



Commandes Curl : 

Create Annonce :
curl -X POST -H "Content-Type: application/json" -d '{"id": "4", "brand": "1", "model": "1", "price": 1}' http://localhost:8080/annonces

Get Annonce by Id :
curl http://localhost:8080/annonces/1

Get All Annonces :
curl http://localhost:8080/annonces