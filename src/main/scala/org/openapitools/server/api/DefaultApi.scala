package org.openapitools.server.api

import org.apache.pekko.http.scaladsl.server.Directives._
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.http.scaladsl.model.StatusCodes
import org.apache.pekko.http.scaladsl.marshalling.ToEntityMarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import org.apache.pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller
import org.openapitools.server.AkkaHttpHelper._
import org.openapitools.server.model.Annonces


class AnnoncesApi(
    defaultService: AnnoncesApiService,
    defaultMarshaller: AnnoncesApiMarshaller,
) {

  
  
  import defaultMarshaller._
  import defaultService._

  val route: Route =
    path("annonces" / Segment) { (id) => 
      get {  
            defaultService.AnnonceIdGet(id = id)
      }
    } ~
    path("annonces") { 
      get {  
            defaultService.annoncesGet()
      }
    } ~
    path("annonces") {
      post {
        entity(as[Annonces]) { annonces =>
          defaultService.annoncesPost(
            id = annonces.id,
            brand = annonces.brand,
            model = annonces.model,
            price = annonces.price
          )
        }
      }
    }/*~
    path("annonces" / Segment) { (id) => 
      put {  
            entity(as[Annonces]){ annonces =>
              defaultService.AnnonceIdPut(id = id, annonces = annonces)
            }
      }
    }*/ 
}


trait AnnoncesApiService {

  def AnnonceIdGet200: Route =
    complete((200, "Successful operation."))
  def AnnonceIdGet404: Route =
    complete((404, "Advertisement not found."))
  def AnnonceIdGet(id: String): Route

  def annoncesGet200: Route =
    complete((200, "Successful operation."))
  def annoncesGet404: Route =
    complete((404, "No advertisements found."))
  def annoncesGet(): Route

  def annoncesPost201: Route =
    complete((201, "Successful creation of an advertisement."))
  def annoncesPost400: Route =
    complete((400, "Invalid request."))
  def annoncesPost(id: String,brand: String, model: String, price: Float): Route
  /*
  def AnnonceIdPut200: Route =
    complete((200, "Successful update of the advertisement."))
  def AnnonceIdPut404: Route =
    complete((404, "Advertisement not found."))

  
  def AnnonceIdPut(id: String, annonces: Annonces): Route
  */
  

}

trait AnnoncesApiMarshaller {
  implicit def fromEntityUnmarshallerAnnonces: FromEntityUnmarshaller[Annonces]

 implicit def toEntityMarshallerAnnoncesarray: ToEntityMarshaller[Seq[Annonces]]

  implicit def toEntityMarshallerAnnonces: ToEntityMarshaller[Annonces]


}

