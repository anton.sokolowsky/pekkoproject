package org.openapitools.server.model


/**
 * @param id    The id of the phone. 
 * @param brand The brand of the phone. 
 * @param model The model of the phone. 
 * @param price The price of the phone. 
*/
final case class Annonces(
  id: String,
  brand: String,
  model: String,
  price: Float
)

