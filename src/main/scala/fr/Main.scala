package org.openapitools.server.api

import org.apache.pekko

import pekko.actor.typed.scaladsl.AskPattern._

import pekko.http.scaladsl.model.StatusCodes

import org.openapitools.server.model.Annonces
import org.openapitools.server
import org.openapitools.server.api.AnnoncesActor
import org.openapitools.server.api.AnnoncesActor._

import pekko.http.scaladsl.marshalling.ToEntityMarshaller
import pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller

import pekko.actor.typed.{ActorSystem, ActorRef}
import pekko.actor.typed.scaladsl.Behaviors

import pekko.http.scaladsl.Http
import pekko.http.scaladsl.server.Route
import pekko.http.scaladsl.server.StandardRoute
import pekko.http.scaladsl.server.RouteResult

import pekko.http.scaladsl.server.Directives._
import pekko.http.scaladsl.model.StatusCodes
import pekko.http.scaladsl.marshalling.ToEntityMarshaller
import pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller

import pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._

import scala.io.StdIn

import pekko.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

import org.openapitools.server.api._

import pekko.actor.typed.scaladsl.AskPattern._

import pekko.actor.typed.Behavior
import spray.json.RootJsonFormat
import scala.util.Failure
import scala.util.Success



object Main extends App {
  implicit val system: ActorSystem[Command] = ActorSystem(AnnoncesActor(), "annonceur")
  implicit val executionContext: ExecutionContext = system.executionContext

  
  val annoncesActor: ActorRef[AnnoncesActor.Command] = system

  def routeFromFutureRoute(futureroute: Future[Route]): Route = (context) => futureroute.flatMap(route => route(context)) 

  // Classe pour représenter la réponse JSON
  case class AnnonceResponse(id: String, brand: String, model: String, price: Float)
  case class AnnoncesResponse(annonces: Seq[AnnonceResponse])

  // Format JSON implicite pour la classe AnnonceResponse
  implicit val annonceResponseFormat: RootJsonFormat[AnnonceResponse] = jsonFormat4(AnnonceResponse)
  implicit val annoncesResponseFormat: RootJsonFormat[AnnoncesResponse] = jsonFormat1(AnnoncesResponse)


  object Api extends AnnoncesApiService {
    implicit val timeout: Timeout = 20.seconds
    
    override def annoncesPost(id: String, brand: String, model: String, price: Float): Route = routeFromFutureRoute {
      print("JE PASSE ICI.")
      annoncesActor.ask(ref => AnnoncesActor.CreateAnnonce(id, brand, model, price, ref)).map {
        case true => complete((201, "Annonce created successfully"))
        case false => complete((400, "Failed to create annonce"))
      }
    }

    override def AnnonceIdGet(id: String): Route = routeFromFutureRoute {
      annoncesActor.ask(ref => AnnoncesActor.GetAnnonceById(id, ref)).map { ma =>
        (ma match {
        case Some(annonce) => 
          val annonceResponse = AnnonceResponse(annonce.id, annonce.brand, annonce.model, annonce.price)
          complete((200, annonceResponse))
        case None => complete((404, s"Annonce with ID $id not found"))
      })
      }
    }

    override def annoncesGet(): Route = routeFromFutureRoute {
      annoncesActor.ask(ref => AnnoncesActor.GetAllAnnonces(ref)).map {
        case Some(annonces) => 
          val annoncesResponse = AnnoncesResponse(annonces.map(a => AnnonceResponse(a.id, a.brand, a.model, a.price)))
          complete((200, annoncesResponse))
        case None => complete((404, "No annonces found"))
      }
    }

    //put pas fait

    
  }

  object Marsh extends AnnoncesApiMarshaller {
    implicit val r = jsonFormat4(Annonces.apply)
    override implicit def fromEntityUnmarshallerAnnonces: FromEntityUnmarshaller[Annonces] = r
    override implicit def toEntityMarshallerAnnonces: ToEntityMarshaller[Annonces] = r

    override implicit def toEntityMarshallerAnnoncesarray: ToEntityMarshaller[Seq[Annonces]] = immSeqFormat(r)
  }

  val api = new AnnoncesApi(Api, Marsh)

  val host = "localhost"
  val port = 8080

  val bindingFuture = Http().newServerAt(host, port).bind(api.route)
  println(s"Server online at http://${host}:${port}/\nPress RETURN to stop...")

  bindingFuture.failed.foreach { ex =>
    println(s"${ex} Failed to bind to ${host}:${port}!")
  }

  StdIn.readLine() // let it run until user presses return
  bindingFuture
    .flatMap(_.unbind()) // trigger unbinding from the port
    .onComplete(_ => system.terminate()) // and shutdown when done

}


