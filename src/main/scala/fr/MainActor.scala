import org.apache.pekko
import pekko.http.scaladsl.model.StatusCodes

import org.openapitools.server.model.Annonces
import org.openapitools.server
import org.openapitools.server.api.AnnoncesActor
import org.openapitools.server.api.AnnoncesActor._

import pekko.http.scaladsl.marshalling.ToEntityMarshaller
import pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller

import pekko.actor.typed.{ActorSystem, ActorRef}
import pekko.actor.typed.scaladsl.Behaviors

import pekko.http.scaladsl.Http
import pekko.http.scaladsl.server.Route
import pekko.http.scaladsl.server.StandardRoute
import pekko.http.scaladsl.server.RouteResult

import pekko.http.scaladsl.server.Directives._
import pekko.http.scaladsl.model.StatusCodes
import pekko.http.scaladsl.marshalling.ToEntityMarshaller
import pekko.http.scaladsl.unmarshalling.FromEntityUnmarshaller
import pekko.http.scaladsl.unmarshalling.FromStringUnmarshaller

import pekko.http.scaladsl.marshallers.sprayjson.SprayJsonSupport._
import spray.json.DefaultJsonProtocol._

import scala.io.StdIn

import pekko.util.Timeout
import scala.concurrent.duration._
import scala.concurrent.{ExecutionContext, Future}

import org.openapitools.server.api._

import pekko.actor.typed.scaladsl.AskPattern._

import pekko.actor.typed.Behavior
import spray.json.RootJsonFormat
import scala.util.Failure
import scala.util.Success

import scala.concurrent.Await


object ActorDemo extends App {
  implicit val system: ActorSystem[Command] = ActorSystem(AnnoncesActor(), "annonceur")
  implicit val executionContext: ExecutionContext = system.executionContext

  
  val annoncesActor: ActorRef[AnnoncesActor.Command] = system

  def routeFromFutureRoute(futureroute: Future[Route]): Route = (context) => futureroute.flatMap(route => route(context)) 

  // Classe pour représenter la réponse JSON
  case class AnnonceResponse(id: String, brand: String, model: String, price: Float)
  case class AnnoncesResponse(annonces: Seq[AnnonceResponse])

  // Format JSON implicite pour la classe AnnonceResponse
  implicit val annonceResponseFormat: RootJsonFormat[AnnonceResponse] = jsonFormat4(AnnonceResponse)
  implicit val annoncesResponseFormat: RootJsonFormat[AnnoncesResponse] = jsonFormat1(AnnoncesResponse)


  implicit val timeout: Timeout = 20.seconds

  // Define a method to await the result of an actor message

    var id: String = "1"    
    var result1 = Await.result(annoncesActor.ask(ref => AnnoncesActor.GetAnnonceById(id, ref)), 5.seconds)

    id = "2"
    var result2 = Await.result(annoncesActor.ask(ref => AnnoncesActor.GetAnnonceById(id, ref)), 5.seconds)

    id = "3"
    var result3 = Await.result(annoncesActor.ask(ref => AnnoncesActor.GetAnnonceById(id, ref)), 5.seconds)


    
    var result4 = Await.result(annoncesActor.ask(ref => AnnoncesActor.GetAllAnnonces(ref)), 5.seconds)
    id = "4"
    var brand: String = "brand"
    var model: String = "Model4"
    var price: Float = 1500.0f
    var result5 = Await.result(annoncesActor.ask(ref => AnnoncesActor.CreateAnnonce(id, brand, model, price, ref)), 5.seconds)


    var result6 = Await.result(annoncesActor.ask(ref => AnnoncesActor.GetAllAnnonces(ref)), 5.seconds)
  

  // Print the results
  println(s"Result of GetAnnonceById: $result1")
  println(s"Result of GetAnnonceById: $result2")
  println(s"Result of GetAnnonceById: $result3")

  println(s"Result of GetAllAnnonces: $result4")
  println(s"Result of CreateAnnonce: $result5")
  println(s"Result of GetAllAnnonces: $result6")

  // Shutdown the actor system
  system.terminate()
}
