package org.openapitools.server.api

import org.apache.pekko
import org.apache.pekko.actor.typed.scaladsl.Behaviors
import org.apache.pekko.http.scaladsl.server.Route
import org.apache.pekko.http.scaladsl.server.Directives._

import pekko.actor.typed.{ActorRef, Behavior,ActorSystem}
import pekko.actor.typed.scaladsl.{Behaviors,AbstractBehavior,ActorContext}
import scala.util.Random


// Define a case class representing a phone advertisement

final case class Annonce(id: String, brand: String, model: String, price: Float)

object AnnoncesActor {

  // Define messages for the actor
  sealed trait Command
  case class GetAnnonceById(id: String, replyTo: ActorRef[Option[Annonce]]) extends Command
  case class GetAllAnnonces(replyTo: ActorRef[Option[List[Annonce]]]) extends Command
  case class CreateAnnonce(id: String,brand: String, model: String, price: Float, replyTo: ActorRef[Boolean]) extends Command

  // Behavior for the actor
  def apply(): Behavior[Command] = Behaviors.setup(context => new AnnoncesActor(context))
}

  // Inner class for the behavior
class AnnoncesActor(context: ActorContext[AnnoncesActor.Command]) extends AbstractBehavior[AnnoncesActor.Command](context) {

  context.log.info("Hello AnnoncesActor !")
  import AnnoncesActor._

  var annonces: Map[String, Annonce] = Map("1" -> Annonce("1", "Brand1", "Model1", 1000.0f),"2" -> Annonce("2", "Brand2", "Model2", 1000.0f),"3" -> Annonce("3", "Brand3", "Model3", 1000.0f))
  
  override def onMessage(msg: Command): Behavior[Command] =
    msg match {
      case GetAnnonceById(id, replyTo) =>
        context.log.info(s"GetAnnonce with the ID:$id")
        replyTo ! annonces.get(id)
        this

      case GetAllAnnonces(replyTo) =>
        context.log.info(s"GetAllAnnonces")
        replyTo ! Some(annonces.values.toList)
        this

      case CreateAnnonce(id: String, brand: String, model: String, price: Float, replyTo) =>
        replyTo ! {
          if (annonces.contains(id)) {
            context.log.info(s"Error: Annonce with ID $id already exists.")
            false
          } else {
            val newAnnonce = Annonce(id, brand, model, price)  // Assuming Annonce is a case class
            context.log.info(s"Adding Annonce: $newAnnonce")
            annonces += (id -> newAnnonce)
            true
          }
        }
        this
      }

      //put pas fait
}     