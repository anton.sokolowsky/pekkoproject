```mermaid
sequenceDiagram
  participant Client
  participant AnnoncesApi
  participant AnnoncesActor

  Client ->> AnnoncesApi: GET /annonces
  AnnoncesApi ->> AnnoncesActor: GetAllAnnonces(ref)
  AnnoncesActor -->> AnnoncesApi: Option[List[Annonce]]
  AnnoncesApi -->> Client: 200 OK (with Annonces data) or 404 Not Found